public class Drivability
{
    private double windSpeed;
    private double visibility;
    private double precipitation;
    private double temperature;

    private final double lowestWindSpeed = 38.53;
    private final double highestWindSpeed = 55.3;

    private final double lowestVisibility = 1.0;
    private final double highestVisibility = 7.0;

    private final double lowestPrecipitation = 30;
    private final double highestPrecipitation = 90;

    private final double highestTemperature = 242.5;

    private final double lowestLTemperature = 28;
    private final double highestLTemperature = -20;

    public Drivability( double windSpeed, double visibility, double precipitation, double temperature )
    {
        this.windSpeed = windSpeed;
        this.visibility = visibility;
        this.precipitation = precipitation;
        this.temperature = temperature;
    }

    public double calculate()
    {
        double drivability = 1;
        if( this.windSpeed >= lowestWindSpeed && drivability > 0 )
        {
            double minWindSpeed = ( this.windSpeed - lowestWindSpeed )/( highestWindSpeed - lowestWindSpeed );
            drivability -= minWindSpeed;
        }

        if( this.visibility <= highestVisibility && drivability > 0 )
        {
            double minVisibility = ( highestVisibility - this.visibility )/( highestVisibility - lowestVisibility );
            drivability -= minVisibility;
        }

        if( this.precipitation >= lowestPrecipitation && drivability > 0 )
        {
            double minPrecipitation = 0.4 * ( this.precipitation - lowestPrecipitation )/( highestPrecipitation - lowestPrecipitation );
            drivability -= minPrecipitation;
        }

        if( this.temperature >= highestTemperature ) drivability -= 1;

        if( this.temperature <= highestLTemperature )
        {
            double minLTemperature = 0.95 * ( highestLTemperature - this.temperature )/( highestLTemperature - lowestLTemperature );
            drivability -= minLTemperature;
        }

        if( drivability < 0 ) drivability = 0;

        return drivability;
    }

    //public static void main(String[] args)
    //{
    //    Drivability d = new Drivability( 39,Chi 0.28, 0.4, 18 );
    //    System.out.println( d.calculate() );
    //}
}