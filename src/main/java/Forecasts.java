public class Forecasts
{
    private String ironPhrase;
    private double temperature;
    private double WindSpeed;
    private double visibility;
    private double humidity;

    public void setIronPhrase(String ironPhrase) {
        this.ironPhrase = ironPhrase;
    }

    public String getIronPhrase() {
        return ironPhrase;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setWindSpeed(double windSpeed) {
        WindSpeed = windSpeed;
    }

    public double getWindSpeed() {
        return WindSpeed;
    }

    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    public double getVisibility() {
        return visibility;
    }

    public void setHumidity(double cloudCover) {
        this.humidity = cloudCover;
    }

    public double getHumidity() {
        return humidity;
    }
}
