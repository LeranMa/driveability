/**
 * @author Yulong Yang
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;

public class MainFX extends Application
{
    private TextField Position;
    private TextField Destination;

    private Label Output;

    private Label Drivability;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Label Posi = new Label( "Please enter your position city:      " );
        Label Dest = new Label( "Please enter your destination city: " );

        Position = new TextField( );
        Destination = new TextField( );

        Button Confirm = new Button( "Confirm" );
        Confirm.setOnAction( new ButtonConfirmHandler() );

        HBox hb1 = new HBox( 10, Posi, Position );
        HBox hb2 = new HBox( 10, Dest, Destination );
        VBox vhb12 = new VBox( 10, hb1, hb2 );

        HBox hvb = new HBox( 10, vhb12, Confirm );
        hvb.setAlignment( Pos.CENTER );

        Output = new Label( );

        Drivability = new Label( "Drivability: " );

        VBox vBox = new VBox( 10, hvb, Output, Drivability );
        vBox.setAlignment( Pos.CENTER );

        Scene scene = new Scene( vBox );

        primaryStage.setScene( scene );
        primaryStage.setMinHeight( 1000 );
        primaryStage.setMinWidth( 1000 );
        primaryStage.setTitle( "HackPSUProgram" );
        primaryStage.show();
    }

    public static void main(String[] args)
    {
        launch( args );
    }

    public class ButtonConfirmHandler implements EventHandler<ActionEvent>
    {
        @Override
        public void handle(ActionEvent event)
        {
            String city = Position.getText();
            String city2 = Destination.getText();

            city = city.replace(" ", "%20");
            city2 = city2.replace(" ", "%20");

            CloseableHttpResponse rest = null;

            String url1 = "https://maps.googleapis.com/maps/api/directions/json?origin=" + city + "&destination=" + city2 + "&key=AIzaSyDP5sCRyXFe8hRIWDEKcrlxmP9FItdzMXE";

            CloseableHttpClient client = HttpClients.createDefault();

            HttpGet get1 = new HttpGet(url1);

            String string1 = "";

            try
            {
                rest = client.execute(get1);
                HttpEntity entity = rest.getEntity();
                string1 = EntityUtils.toString( entity );
            }
            catch (IOException ioe)
            {
                System.err.println("Wrong");
                ioe.printStackTrace();
            }
            catch (Exception e)
            {
                System.err.println("Wrong");
            }

            int start = 0;
            for (int i=0; i<string1.length()-16; i++)
            {
                if (string1.substring(i, i + 16).equals("\"start_location\""))
                {
                    start = i;
                    break;
                }
            }

            String start_lat = "";
            int sstart=0;
            for (int i=start; i<string1.length()-8; i++)
            {
                if (string1.substring(i, i + 5).equals("\"lat\""))
                {
                    sstart = i + 8;

                    for (int j = sstart; j < string1.length(); j++) {
                        if (string1.charAt(j) == ',') {
                            start_lat = string1.substring(sstart, j);
                            start = j;
                            break;
                        }
                    }
                }
                if (!start_lat.equals(""))
                {
                    break;
                }
            }

            String start_lng = "";
            for (int i=start; i<string1.length()-8; i++)
            {
                if (string1.substring(i, i + 5).equals("\"lng\"")) {
                    sstart = i + 8;

                    for (int j = sstart; j < string1.length(); j++) {
                        if (string1.charAt(j) == '}') {
                            start_lng = string1.substring(sstart, j).trim();
                            start = j;
                            break;
                        }
                    }
                }
                if (!start_lng.equals(""))
                {
                    break;
                }
            }

            for (int i=start; i<string1.length()-7; i++)
            {
                if (string1.substring(i, i + 7).equals("\"steps\""))
                {
                    start = i;
                    break;
                }
            }

            ArrayList<String> locations_lat = new ArrayList<String>();
            for (int i=start; i<string1.length()-8; i++)
            {
                if (string1.substring(i, i + 5).equals("\"lat\"")) {
                    sstart = i + 8;

                    for (int j = sstart; j < string1.length(); j++) {
                        if (string1.charAt(j) == ',') {
                            locations_lat.add(string1.substring(sstart, j));
                            break;
                        }
                    }
                }
            }

            ArrayList<String> locations_lng = new ArrayList<String>();
            for (int i=start; i<string1.length()-8; i++)
            {
                if (string1.substring(i, i + 5).equals("\"lng\"")) {
                    sstart = i + 8;

                    for (int j = sstart; j < string1.length(); j++) {
                        if (string1.charAt(j) == '}') {
                            locations_lng.add(string1.substring(sstart, j).trim());
                            break;
                        }
                    }
                }
            }

            ArrayList<String> zips = new ArrayList<String>();
            ArrayList<String> cities = new ArrayList<String>();
            ArrayList<String> times = new ArrayList<String>();
            for (int i=0; i<locations_lat.size(); i++)
            {
                zips.add(new TravelTime().getZip(start_lat,start_lng,locations_lat.get(i),locations_lng.get(i)));
                cities.add(new TravelTime().getCity(start_lat,start_lng,locations_lat.get(i),locations_lng.get(i)));
                times.add(new TravelTime().getTime(start_lat,start_lng,locations_lat.get(i),locations_lng.get(i)));
            }

            for (int i=1; i<cities.size(); i+=2)
            {
                cities.remove(i);
                zips.remove(i);
                times.remove(i);
                i--;
            }

            for (int i=1; i<cities.size(); i++)
            {
                if (cities.get(i).equals(cities.get(i-1)))
                {
                    cities.remove(i);
                    zips.remove(i);
                    times.remove(i);
                    i--;
                }
            }

            //ArrayList<String> weather =  new ArrayList<String>();

            /*for (int i = 0; i < zips.size(); i++)
            {
                Weather.getWeather( zips.get(i) );
            }*/

            double windSpeed, visibility, precipitation, temperature;

            double d = 0;
            int count = 0;

            String out = new String();
            for (int i=0; i<zips.size(); i++)
            {
                count++;
                Weather w = new Weather( zips.get(i) );
                windSpeed = w.getWindSpeed();
                visibility = w.getVisibility();
                precipitation = w.getPrecipitation();
                temperature = w.getTemperature();
                Drivability dd = new Drivability( windSpeed, visibility, precipitation, temperature );
                Warning ww = new Warning( windSpeed, visibility, precipitation, temperature );
                String w1 = ww.WindSpeedWarning();
                String w2 = ww.VisibilityWarning();
                String w3 = ww.PrecipitationWarning();
                String w4 = ww.TemperatureWarning();
                d += dd.calculate();

                out+=zips.get(i)+"  ";
                out+=cities.get(i)+"  ";
                out+=times.get(i)+"  ";
                out+="wind speed: "+windSpeed+"mph  ";
                out+="visibility: "+visibility+"mi  ";
                out+="precipitation: "+precipitation+"%  ";
                out+="temperature: "+temperature+"°F\n";
                if(w1!="") out+=w1+"\n";
                if(w2!="") out+=w2+"\n";
                if(w3!="") out+=w3+"\n";
                if(w4!="") out+=w4+"\n";
                out+="\n";
                System.out.println(zips.get(i));
                System.out.println(cities.get(i));
                System.out.println(times.get(i));
            }
            Output.setText( out );

            Drivability.setText( "Drivability: "+String.format("%.2f",100*d/count)+"%" );

        }

        public String getZip(String lat1, String lng1, String lat2, String lng2)
        {
            CloseableHttpResponse rest = null;

            String url1 = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + lat1 + "," + lng1 + "&destinations=" + lat2 + "%2C" + lng2 + "%7C&key=AIzaSyDP5sCRyXFe8hRIWDEKcrlxmP9FItdzMXE";

            CloseableHttpClient client = HttpClients.createDefault();

            HttpGet get1 = new HttpGet(url1);

            String string1 = "";

            try
            {
                rest = client.execute(get1);
                HttpEntity entity = rest.getEntity();
                string1 = EntityUtils.toString( entity );
            }
            catch (IOException ioe)
            {
                System.err.println("Wrong");
                ioe.printStackTrace();
            }
            catch (Exception e)
            {
                System.err.println("Wrong");
            }

            for (int i=0; i<string1.length()-4; i++)
            {
                if (string1.substring(i, i + 5).equals(", USA"))
                {
                    return string1.substring(i-5, i);
                }
            }
            return null;
        }

        public String getCity(String lat1, String lng1, String lat2, String lng2)
        {
            CloseableHttpResponse rest = null;

            String url1 = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + lat1 + "," + lng1 + "&destinations=" + lat2 + "%2C" + lng2 + "%7C&key=AIzaSyDP5sCRyXFe8hRIWDEKcrlxmP9FItdzMXE";

            CloseableHttpClient client = HttpClients.createDefault();

            HttpGet get1 = new HttpGet(url1);

            String string1 = "";

            try
            {
                rest = client.execute(get1);
                HttpEntity entity = rest.getEntity();
                string1 = EntityUtils.toString( entity );
            }
            catch (IOException ioe)
            {
                System.err.println("Wrong");
                ioe.printStackTrace();
            }
            catch (Exception e)
            {
                System.err.println("Wrong");
            }

            for (int i=0; i<string1.length()-2; i++)
            {
                if (string1.substring(i, i + 2).equals(", "))
                {
                    string1 =  string1.substring(i+2);
                    break;
                }
            }

            for (int i=0; i<string1.length()-2; i++)
            {
                if (string1.substring(i, i + 2).equals(", "))
                {
                    return string1.substring(0,i);
                }
            }
            return null;
        }

        public String getTime(String lat1, String lng1, String lat2, String lng2)
        {
            CloseableHttpResponse rest = null;

            String url1 = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + lat1 + "," + lng1 + "&destinations=" + lat2 + "%2C" + lng2 + "%7C&key=AIzaSyDP5sCRyXFe8hRIWDEKcrlxmP9FItdzMXE";

            CloseableHttpClient client = HttpClients.createDefault();

            HttpGet get1 = new HttpGet(url1);

            String string1 = "";

            try
            {
                rest = client.execute(get1);
                HttpEntity entity = rest.getEntity();
                string1 = EntityUtils.toString( entity );
            }
            catch (IOException ioe)
            {
                System.err.println("Wrong");
                ioe.printStackTrace();
            }
            catch (Exception e)
            {
                System.err.println("Wrong");
            }

            for (int i=0; i<string1.length()-10; i++)
            {
                if (string1.substring(i, i + 10).equals("\"duration\""))
                {
                    string1 =  string1.substring(i+10);
                    break;
                }
            }

            for (int i=0; i<string1.length()-9; i++)
            {
                if (string1.substring(i, i + 4).equals("text"))
                {
                    string1 =  string1.substring(i+9);
                    break;
                }
            }

            for (int i=0; i<string1.length()-2; i++)
            {
                if (string1.substring(i, i + 2).equals("\","))
                {
                    return string1.substring(0,i);
                }
            }
            return null;
        }

    }
}
