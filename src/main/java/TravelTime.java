import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class TravelTime
{
    //public static void main(String[] args)
    //{
    //    doHttpGeto();
    //}

    public static void doHttpGeto()
    {
        System.out.println( "First city: " );
        Scanner scanner = new Scanner( System.in );
        String city = scanner.nextLine();
        city = city.replace(" ", "%20");
        System.out.println( "Second city: " );
        String city2 = scanner.nextLine();
        city2 = city2.replace(" ", "%20");

        CloseableHttpResponse rest = null;

        String url1 = "https://maps.googleapis.com/maps/api/directions/json?origin=" + city + "&destination=" + city2 + "&key=AIzaSyDP5sCRyXFe8hRIWDEKcrlxmP9FItdzMXE";

        CloseableHttpClient client = HttpClients.createDefault();

        HttpGet get1 = new HttpGet(url1);

        String string1 = "";

        try
        {
            rest = client.execute(get1);
            HttpEntity entity = rest.getEntity();
            string1 = EntityUtils.toString( entity );
        }
        catch (IOException ioe)
        {
            System.err.println("Wrong");
            ioe.printStackTrace();
        }
        catch (Exception e)
        {
            System.err.println("Wrong");
        }

        int start = 0;
        for (int i=0; i<string1.length()-16; i++)
        {
            if (string1.substring(i, i + 16).equals("\"start_location\""))
            {
                start = i;
                break;
            }
        }

        String start_lat = "";
        int sstart=0;
        for (int i=start; i<string1.length()-8; i++)
        {
            if (string1.substring(i, i + 5).equals("\"lat\""))
            {
                sstart = i + 8;

                for (int j = sstart; j < string1.length(); j++) {
                    if (string1.charAt(j) == ',') {
                        start_lat = string1.substring(sstart, j);
                        start = j;
                        break;
                    }
                }
            }
            if (!start_lat.equals(""))
            {
                break;
            }
        }

        String start_lng = "";
        for (int i=start; i<string1.length()-8; i++)
        {
            if (string1.substring(i, i + 5).equals("\"lng\"")) {
                sstart = i + 8;

                for (int j = sstart; j < string1.length(); j++) {
                    if (string1.charAt(j) == '}') {
                        start_lng = string1.substring(sstart, j).trim();
                        start = j;
                        break;
                    }
                }
            }
            if (!start_lng.equals(""))
            {
                break;
            }
        }

        for (int i=start; i<string1.length()-7; i++)
        {
            if (string1.substring(i, i + 7).equals("\"steps\""))
            {
                start = i;
                break;
            }
        }

        ArrayList<String> locations_lat = new ArrayList<String>();
        for (int i=start; i<string1.length()-8; i++)
        {
            if (string1.substring(i, i + 5).equals("\"lat\"")) {
                sstart = i + 8;

                for (int j = sstart; j < string1.length(); j++) {
                    if (string1.charAt(j) == ',') {
                        locations_lat.add(string1.substring(sstart, j));
                        break;
                    }
                }
            }
        }

        ArrayList<String> locations_lng = new ArrayList<String>();
        for (int i=start; i<string1.length()-8; i++)
        {
            if (string1.substring(i, i + 5).equals("\"lng\"")) {
                sstart = i + 8;

                for (int j = sstart; j < string1.length(); j++) {
                    if (string1.charAt(j) == '}') {
                        locations_lng.add(string1.substring(sstart, j).trim());
                        break;
                    }
                }
            }
        }

        ArrayList<String> zips = new ArrayList<String>();
        ArrayList<String> cities = new ArrayList<String>();
        ArrayList<String> times = new ArrayList<String>();
        for (int i=0; i<locations_lat.size(); i++)
        {
            zips.add(new TravelTime().getZip(start_lat,start_lng,locations_lat.get(i),locations_lng.get(i)));
            cities.add(new TravelTime().getCity(start_lat,start_lng,locations_lat.get(i),locations_lng.get(i)));
            times.add(new TravelTime().getTime(start_lat,start_lng,locations_lat.get(i),locations_lng.get(i)));
        }

        for (int i=1; i<cities.size(); i+=2)
        {
            cities.remove(i);
            zips.remove(i);
            times.remove(i);
            i--;
        }

        for (int i=1; i<cities.size(); i++)
        {
            if (cities.get(i).equals(cities.get(i-1)))
            {
                cities.remove(i);
                zips.remove(i);
                times.remove(i);
                i--;
            }
        }

        for (int i=0; i<zips.size(); i++)
        {
            System.out.println(zips.get(i));
            System.out.println(cities.get(i));
            System.out.println(times.get(i));
        }

        //ArrayList<String> weather =  new ArrayList<String>();

       /*for (int i = 0; i < zips.size(); i++)
       {
           Weather.getWeather( zips.get(i) );
       }*/
    }

    public String getZip(String lat1, String lng1, String lat2, String lng2)
    {
        CloseableHttpResponse rest = null;

        String url1 = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + lat1 + "," + lng1 + "&destinations=" + lat2 + "%2C" + lng2 + "%7C&key=AIzaSyDP5sCRyXFe8hRIWDEKcrlxmP9FItdzMXE";

        CloseableHttpClient client = HttpClients.createDefault();

        HttpGet get1 = new HttpGet(url1);

        String string1 = "";

        try
        {
            rest = client.execute(get1);
            HttpEntity entity = rest.getEntity();
            string1 = EntityUtils.toString( entity );
        }
        catch (IOException ioe)
        {
            System.err.println("Wrong");
            ioe.printStackTrace();
        }
        catch (Exception e)
        {
            System.err.println("Wrong");
        }

        for (int i=0; i<string1.length()-4; i++)
        {
            if (string1.substring(i, i + 5).equals(", USA"))
            {
                return string1.substring(i-5, i);
            }
        }
        return null;
    }

    public String getCity(String lat1, String lng1, String lat2, String lng2)
    {
        CloseableHttpResponse rest = null;

        String url1 = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + lat1 + "," + lng1 + "&destinations=" + lat2 + "%2C" + lng2 + "%7C&key=AIzaSyDP5sCRyXFe8hRIWDEKcrlxmP9FItdzMXE";

        CloseableHttpClient client = HttpClients.createDefault();

        HttpGet get1 = new HttpGet(url1);

        String string1 = "";

        try
        {
            rest = client.execute(get1);
            HttpEntity entity = rest.getEntity();
            string1 = EntityUtils.toString( entity );
        }
        catch (IOException ioe)
        {
            System.err.println("Wrong");
            ioe.printStackTrace();
        }
        catch (Exception e)
        {
            System.err.println("Wrong");
        }

        for (int i=0; i<string1.length()-2; i++)
        {
            if (string1.substring(i, i + 2).equals(", "))
            {
                string1 =  string1.substring(i+2);
                break;
            }
        }

        for (int i=0; i<string1.length()-2; i++)
        {
            if (string1.substring(i, i + 2).equals(", "))
            {
                return string1.substring(0,i);
            }
        }
        return null;
    }

    public String getTime(String lat1, String lng1, String lat2, String lng2)
    {
        CloseableHttpResponse rest = null;

        String url1 = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + lat1 + "," + lng1 + "&destinations=" + lat2 + "%2C" + lng2 + "%7C&key=AIzaSyDP5sCRyXFe8hRIWDEKcrlxmP9FItdzMXE";

        CloseableHttpClient client = HttpClients.createDefault();

        HttpGet get1 = new HttpGet(url1);

        String string1 = "";

        try
        {
            rest = client.execute(get1);
            HttpEntity entity = rest.getEntity();
            string1 = EntityUtils.toString( entity );
        }
        catch (IOException ioe)
        {
            System.err.println("Wrong");
            ioe.printStackTrace();
        }
        catch (Exception e)
        {
            System.err.println("Wrong");
        }

        for (int i=0; i<string1.length()-10; i++)
        {
            if (string1.substring(i, i + 10).equals("\"duration\""))
            {
                string1 =  string1.substring(i+10);
                break;
            }
        }

        for (int i=0; i<string1.length()-9; i++)
        {
            if (string1.substring(i, i + 4).equals("text"))
            {
                string1 =  string1.substring(i+9);
                break;
            }
        }

        for (int i=0; i<string1.length()-2; i++)
        {
            if (string1.substring(i, i + 2).equals("\","))
            {
                return string1.substring(0,i);
            }
        }
        return null;
    }
}