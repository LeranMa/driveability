public class Warning
{
    private double windSpeed;
    private double visibility;
    private double precipitation;
    private double temperature;

    private final double windS8 = 38.53;
    private final double windS9 = 46.6;
    private final double windS10 = 55.3;

    /*private final double visS1 = 0.31069;
    private final double visS2 = 0.12427;
    private final double visS3 = 0.06214;
    private final double visS4 = 0.03107;
    private final double visS5 = 0.01243;*/

    private final double visS1 = 6;
    private final double visS2 = 5;
    private final double visS3 = 4;
    private final double visS4 = 3;
    private final double visS5 = 2;

    private final double HTemp = 242.5;
    private final double LTemp = 28;

    private final double preS9 = 90;
    private final double preS7 = 70;

    public Warning( double windSpeed, double visibility, double precipitation, double temperature )
    {
        this.windSpeed = windSpeed;
        this.visibility = visibility;
        this.precipitation = precipitation;
        this.temperature = temperature;
    }

    public String WindSpeedWarning( )
    {
        if( this.windSpeed >= windS10 ) return "Whole gale: Seldom experienced inland; trees uprooted; considerable structural damage occurs.";
        else if( this.windSpeed >= windS9 ) return "Strong gale: Slight structural damage occurs (chimney pots and roof tiles removed.";
        else if( this.windSpeed >= windS8 ) return "Fresh gale: Breaks twigs off trees; generally impedes progress. ";
        return "";
    }

    public String VisibilityWarning( )
    {
        if( this.visibility <= visS5 ) return "The visibility is " + this.visibility + " miles. please do not drive. ";
        else if( this.visibility <= visS4 ) return "The visibility is " + this.visibility + " miles. Please turn on the fog lights. Please don’t enter the motorway. ";
        else if( this.visibility <= visS3 ) return "The visibility is " + this.visibility + " miles. Please turn on the fog lights. Please slow down. ";
        else if( this.visibility <= visS2 ) return "The visibility is " + this.visibility + " miles. Please turn on the fog lights. ";
        else if( this.visibility <= visS1 ) return "The visibility is " + this.visibility + " miles. Please maintain 150 meters or more space with the front car. ";
        return "";
    }

    public String TemperatureWarning( )
    {
        if( this.temperature >= HTemp ) return "The temperature is extremely high, please do not drive. ";
        if( this.HTemp <= LTemp ) return "The temperature is **, The low temperature may affect your driving. ";
        return "";
    }

    public String PrecipitationWarning( )
    {
        if( this.precipitation > preS9 ) return "prompt: The precipitation is higher than 90%";
        else if( this.precipitation > preS7 ) return "prompt: The precipitation is higher than 70%";
        return "";
    }

    //public static void main(String[] args)
    //{
    //    Warning d = new Warning( 39, 0.28, 0.4, 18 );
    //    System.out.println(d.PrecipitationWarning()+d.TemperatureWarning()+d.WindSpeedWarning()+d.VisibilityWarning());
    //}
}