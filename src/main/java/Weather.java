import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Calendar;

public class Weather
{
    private int hour;
    private double windSpeed;
    private double visibility;
    private double precipitation;
    private double temperature;

    static Forecasts [] hours = new Forecasts[12];

    //public static void getWeather( String zip )
    public Weather( String zip )
    {
        CloseableHttpResponse rest = null;

        CloseableHttpClient client = HttpClients.createDefault();

        String string1 = "";
        String string2 = "";

        for (int i = 0; i < 12; i++)
        {
            hours[i] = new Forecasts();
        }

        String url = "http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/"+zip+"?apikey=ha5CPJyBNCDASBIih0ZDxI11L1y8FyGu&details=true";
        HttpGet get = new HttpGet( url );

        try
        {
            rest = client.execute( get );
            HttpEntity entity = rest.getEntity();
            String string3 = EntityUtils.toString( entity );
            //System.out.println( string3 );

            int l = string3.length();
            //System.out.println( l );
            int count = 0;
            for (int i = 0; i < l-10; i++)
            {
                String iconPhrase = string3.substring( i, i+10 );
                String phrase = "";
                if( iconPhrase.equalsIgnoreCase( "IconPhrase" ) )
                {
                    for (int j = i+13; j < l; j++) {
                        String next = string3.substring( j,j+1 );
                        if( !next.equalsIgnoreCase("\"") )
                        {
                            phrase += next;
                        }
                        else break;
                    }
                    hours[count].setIronPhrase( phrase );
                    phrase = "";
                    count++;
                }
            }

            count = 0;
            for (int i = 0; i < l-23; i++)
            {
                String iconPhrase = string3.substring( i, i+13 );
                String temp = "";
                if( iconPhrase.equalsIgnoreCase( "\"Temperature\"" ) )
                {
                    for (int j = i+23; j < l; j++) {
                        String next = string3.substring( j,j+1 );
                        if( !next.equalsIgnoreCase(",") )
                        {
                            temp += next;
                        }
                        else break;
                    }
                    double value = Double.parseDouble( temp );
                    hours[count].setTemperature( value );
                    if(count < 11)
                        count++;
                }
            }

            count = 0;
            for (int i = 0; i < l-24; i++)
            {
                String iconPhrase = string3.substring( i, i+13 );
                String temp = "";
                if( iconPhrase.equalsIgnoreCase( "Wind\":{\"Speed" ) )
                {
                    for (int j = i+24; j < l; j++) {
                        String next = string3.substring( j,j+1 );
                        if( !next.equalsIgnoreCase(",") )
                        {
                            temp += next;
                        }
                        else break;
                    }
                    double value = Double.parseDouble( temp );
                    hours[count].setWindSpeed( value );
                    count++;
                }
            }

            count = 0;
            for (int i = 0; i < l-21; i++)
            {
                String iconPhrase = string3.substring( i, i+10 );
                String temp = "";
                if( iconPhrase.equalsIgnoreCase( "Visibility" ) )
                {
                    for (int j = i+21; j < l; j++) {
                        String next = string3.substring( j,j+1 );
                        if( !next.equalsIgnoreCase(",") )
                        {
                            temp += next;
                        }
                        else break;
                    }
                    double value = Double.parseDouble( temp );
                    hours[count].setVisibility( value );
                    count++;
                }
            }

            count = 0;
            for (int i = 0; i < l-10; i++)
            {
                String iconPhrase = string3.substring( i, i+8 );
                String temp = "";
                if( iconPhrase.equalsIgnoreCase( "Humidity" ) )
                {
                    for (int j = i+10; j < l; j++) {
                        String next = string3.substring( j,j+1 );
                        if( !next.equalsIgnoreCase(",") )
                        {
                            temp += next;
                        }
                        else break;
                    }
                    double value = Double.parseDouble( temp );
                    hours[count].setHumidity( value );
                    count++;
                }
            }

            Calendar c = Calendar.getInstance();
            this.hour = c.get(Calendar.HOUR_OF_DAY);

            this.windSpeed = hours[this.hour].getWindSpeed();
            this.visibility = hours[this.hour].getVisibility();
            this.precipitation = hours[this.hour].getHumidity();
            this.temperature = hours[this.hour].getTemperature();

/*            for (int i = 0; i < 12; i++) {
                System.out.println( hours[0].getTemperature() );
            }*/

        }
        catch (IOException ioe)
        {
            System.err.println("Wrong");
            ioe.printStackTrace();
        }
        catch (Exception e)
        {
            System.err.println("Wrong");
        }
    }

    public double getWindSpeed()
    {
        return this.windSpeed;
    }

    public double getVisibility()
    {
        return this.visibility;
    }

    public double getPrecipitation()
    {
        return this.precipitation;
    }

    public double getTemperature()
    {
        return this.temperature;
    }

    //public static void main(String[] args)
    //{
    //     //getWeather(  );
    //}
}


//http://dataservice.accuweather.com/locations/v1/cities/search?apikey=ha5CPJyBNCDASBIih0ZDxI11L1y8FyGu&q=   (+city)